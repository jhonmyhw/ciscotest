import { Component, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import * as cisco from 'src/webex_require_bundle.js';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements AfterViewInit {

  @ViewChild('invitee', { static: false }) invitee: ElementRef;
  @ViewChild('credentials', { static: false }) credentials: ElementRef;
  @ViewChild('dialer', { static: false }) dialer: ElementRef;
  @ViewChild('connectionStatus', { static: false }) connectionStatus: ElementRef;
  @ViewChild('localVideo', { static: false }) localVideo: ElementRef;
  @ViewChild('remoteVideo', { static: false }) remoteVideo: ElementRef;
  @ViewChild('remoteAudio', { static: false }) remoteAudio: ElementRef;
  @ViewChild('localStatus', { static: false }) localStatus: ElementRef;
  @ViewChild('remoteStatus', { static: false }) remoteStatus: ElementRef;
  @ViewChild('hangup', { static: false }) hangup: ElementRef;
  @ViewChild('consVideo', { static: false }) consVideo: ElementRef;
  @ViewChild('consAudio', { static: false }) consAudio: ElementRef;

  constructor() {}

  ngAfterViewInit() {

    this.credentials.nativeElement.addEventListener('submit', (event) => {
      // Let's make sure we don't reload the page when we submit the form
      event.preventDefault();
      // The rest of the incoming call setup happens in connect();
      connect(cisco, this.connectionStatus.nativeElement);
    });

    this.dialer.nativeElement.addEventListener('submit', (event) => {
      // Again, we don't want to reload when we try to dial
      event.preventDefault();

      const destination = this.invitee.nativeElement.value;

      // We'll use `connect()` (even though we might already be connected or
      // connecting) to make sure we've got a functional webex instance.
      connect(cisco, this.connectionStatus.nativeElement)
        .then(function(Webex:any) {
          // Create the meeting
          console.log(Webex);
          console.log(destination);
          Webex.meetings.create(destination).then((meeting) => {
            // Call our helper function for binding events to meetings
            console.log("Meeting has been created!!");
            bindMeetingEvents(meeting, this.localVideo.nativeElement, this.remoteVideo.nativeElement, this.remoteAudio.nativeElement, this.localStatus.nativeElement, this.remoteStatus.nativeElement, this.hangup.nativeElement);
            return joinMeeting(meeting, this.consAudio.nativeElement, this.consVideo.nativeElement);
          });
        })
        .catch((error) => {
          // Report the error
          console.log("CREATE error:");
          console.error(error);

          // Implement error handling here
        });
    });


    // There's a few different events that'll let us know we should initialize
    // Webex and start listening for incoming calls, so we'll wrap a few things
    // up in a function.
    function connect(cisco, connectionStatus) {
      return new Promise((resolve) => {

        const Webex = cisco.webex.init({
          config: {
            meetings: {
              deviceType: 'WEB'
            }
            // Any other sdk config we need
          },
          credentials: {
            access_token: "NWRjZDJkOGItYzFkNi00ZWM4LTgwY2UtZWUxNGJmMzgwNGEzY2ZiZGQ1YjYtN2Jj_PF84_b2ea521d-a4db-4950-a461-ea65a14dbdc0"
          }
        });

        // Listen for added meetings
        Webex.meetings.on('meeting:added', (addedMeetingEvent) => {
          if (addedMeetingEvent.type === 'INCOMING') {
            const addedMeeting = addedMeetingEvent.meeting;
            // Acknowledge to the server that we received the call on our device
            addedMeeting.acknowledge(addedMeetingEvent.type)
              .then(() => {
                if (confirm('Answer incoming call')) {
                  joinMeeting(addedMeeting, this.consAudio.nativeElement, this.consVideo.nativeElement);
                  bindMeetingEvents(addedMeeting, this.localVideo.nativeElement, this.remoteVideo.nativeElement, this.remoteAudio.nativeElement, this.localStatus.nativeElement, this.remoteStatus.nativeElement, this.hangup.nativeElement);
                }
                else {
                  addedMeeting.decline();
                }
              });
          }
        });

        // Register our device with Webex cloud
        if (!Webex.meetings.registered) {
          Webex.meetings.register()
            // Sync our meetings with existing meetings on the server
            .then(() => Webex.meetings.syncMeetings())
            .then(() => {
              // // This is just a little helper for our selenium tests and doesn't
              // // really matter for the example
              // document.body.classList.add('listening');
              connectionStatus.innerHTML = 'connected';
              // Our device is now connected
              resolve(Webex);
            })
            // This is a terrible way to handle errors, but anything more specific is
            // going to depend a lot on your app
            .catch((err) => {
              console.error(err);
              // we'll rethrow here since we didn't really *handle* the error, we just
              // reported it
              throw err;
            });
        }
        else {
          // Device was already connected
          resolve(Webex);
        }
      });
    }

    // Similarly, there are a few different ways we'll get a meeting Object, so let's
    // put meeting handling inside its own function.
    function bindMeetingEvents(meeting, localVideo, remoteVideo, remoteAudio, localStatus, remoteStatus, hangup) {
      // call is a call instance, not a promise, so to know if things break,
      // we'll need to listen for the error event. Again, this is a rather naive
      // handler.
      meeting.on('error', (err) => {
        console.error(err);
      });

      // Handle media streams changes to ready state
      meeting.on('media:ready', (media) => {
        if (!media) {
          return;
        }

        if (media.type === 'local') {
          localVideo.srcObject = media.stream;
        }
        if (media.type === 'remoteVideo') {
          remoteVideo.srcObject = media.stream;
        }
        if (media.type === 'remoteAudio') {
          remoteAudio.srcObject = media.stream;
        }
      });

      // Handle media streams stopping
      meeting.on('media:stopped', (media) => {
        // Remove media streams

        if (media.type === 'local') {
          localVideo.srcObject = null;
        }
        if (media.type === 'remoteVideo') {
          remoteVideo.srcObject = null;
        }
        if (media.type === 'remoteAudio') {
          remoteAudio.srcObject = null;
        }
      });

      // Update participant info
      meeting.members.on('members:update', (delta) => {
        const { full: membersData } = delta;
        const memberIDs = Object.keys(membersData);

        memberIDs.forEach((memberID) => {
          const memberObject = membersData[memberID];
          // Devices are listed in the memberships object.
          // We are not concerned with them in this demo
          if (memberObject.isUser) {
            if (memberObject.isSelf) {
              localStatus.innerHTML = memberObject.status;
            }
            else {
              remoteStatus.innerHTML = memberObject.status;
            }
          }
        });
      });

      // Of course, we'd also like to be able to end the call:
      hangup.addEventListener('click', () => {
        meeting.leave();
      });
    }

    // Join the meeting and add media
    function joinMeeting(meeting, consAudio, consVideo) {
      // Get constraints

      const constraints = {
        audio: consAudio.checked,
        video: consVideo.checked
      };

      return meeting.join().then(() => {
        const mediaSettings = {
          receiveVideo: this.consVideo,
          receiveAudio: this.consAudio,
          receiveShare: false,
          sendVideo: this.consVideo,
          sendAudio: this.consAudio,
          sendShare: false
        };

        return meeting.getMediaStreams(mediaSettings).then((mediaStreams) => {
          const [localStream, localShare] = mediaStreams;

          meeting.addMedia({
            localShare,
            localStream,
            mediaSettings
          });
        });
      });
    }






  }
}